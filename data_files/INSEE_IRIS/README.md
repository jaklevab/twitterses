Socioeconomic data extracted from [IRIS](https://www.insee.fr/fr/statistiques/fichier/2673683/BASE_TD_FILO_DISP_IRIS_2013.xls). 
describing the french population at a lower spatial resolution (50,200 cells compared to the 2,3 M in the INSEE_200m dataset).
Aggregate information is however released without any tampering and contains 350 different sociodemographic variables (compared to the 28 in the INSEE_200m).


Geographic shapefiles need to be downloaded from [here](https://public.opendatasoft.com/explore/dataset/contours-iris-2016/export/).

A more detailed comparison between the two datasets can also be found [here](https://www.geocible.com/autres-news/107-les-donnees-carroyees-pour-toujours-plus-de-precision.html)
