# Hourly net salary according to the social occupational class, gender and age in 2014 measured throughout France
Data extracted from [INSEE Dads 2014](https://www.insee.fr/fr/statistiques/2898172?sommaire=2898178)

. **Gross salary per hour (T01 series)**
-  By age and simplified socioprofessional category **(01)**
- By sector and simplified socioprofessional category **(02)**
- By detailed sector **(03)**
- By establishment size **(04)**
- Gross hourly salary eveolution per sector **(05)**
- By age and PCS (profession and socioprofessional category) : Field restricted to companies of more than 20 employees **(06)**

. **Gross yearly full-time salary per hour (T02 series)**
- By age and simplified socioprofessional category **(01)**
- By sector and simplified socioprofessional category **(02)**
- By detailed sector **(03)**
- By establishment size **(04)**
- Gross hourly salary eveolution per sector **(05)**
- By age and PCS (profession and socioprofessional category) : Field restricted to companies of more than 20 employees **(06)**

**NB**: Data T106.xls and T206.xls extracted from  series [INSEE Dads 2007](https://www.insee.fr/fr/statistiques/2382962?sommaire=2382971&q=t203.xls)
